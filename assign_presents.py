import random
import numpy as np
number_of_people = 50


def make_table_of_connections():
    table_of_connections = []
    table_of_presents = list(range(1, number_of_people + 1))
    for person in list(range(1, number_of_people + 1)):
        present = random.choice(table_of_presents)
        table_of_connections.append([person, present])
        table_of_presents.remove(present)
    return table_of_connections


def check_if_table_is_good(my_table_of_connections):
    i = 1
    count = 0
    while True:
        i = my_table_of_connections[i-1][1]
        if my_table_of_connections[i-1][0] == 1:
            break
        count += 1

    if count >= number_of_people - 2:
        print("Found it!")
        return True
    else:
        return False


def find_the_right_table():
    done_yet = False
    while not done_yet:
        print("Testing table")
        table = make_table_of_connections()
        print(table)
        done_yet = check_if_table_is_good(table)
    return table


my_one_and_only_table = find_the_right_table()
np.savetxt("secret_santa.txt", my_one_and_only_table)
